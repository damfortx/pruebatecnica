import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';
import { EmployeesService } from '../employees.service';
import { Employee } from '../entities/employee.entity';
/**
 * pipe to validate the email when an employee is created or updated
 */
@Injectable()
export class ValidationMailPipe
  implements PipeTransform<Employee, Promise<Employee>> {
  constructor(private readonly employeesService: EmployeesService) {}
  async transform(value: Employee): Promise<Employee> {
    //create the email that the employee should have
    let mail = await this.employeesService.createMail(
      value.firstName.toLowerCase().replace(/\s/g, ''),
      value.lastName.toLowerCase().replace(/\s/g, ''),
      value.id,
    );
    mail += '@cidenet.com.';
    if (value.country === 'Colombia') mail += 'co';
    else mail += 'us';
    //verify the email of the employee was valid
    const validMail = value.mail === mail;

    if (validMail) return value;
    throw new BadRequestException('Correo invalido');
  }
}
