import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';
import { EmployeesService } from '../employees.service';
import { Employee } from '../entities/employee.entity';
/**
 * pipe to validate the document of an employee
 */
@Injectable()
export class ValidationDocumentPipe
  implements PipeTransform<Employee, Promise<Employee>> {
  constructor(private readonly employeesService: EmployeesService) {}
 async transform(value: Employee): Promise<Employee> { 
   
    if (
    await  this.employeesService.verifyDocument(value.documentType, value.document,value.id)
    )
      throw new BadRequestException('Document already used');

    return value;
  }
}
