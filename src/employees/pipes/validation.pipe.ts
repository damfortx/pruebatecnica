import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  BadRequestException,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
/**
 * validate the create-employe-dto
 */
@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  async transform(value: any, { metatype }: ArgumentMetadata) {
    // create a instance of the date to validate
    value.addmissionDate = new Date(value.addmissionDate);

    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    //validate the incoming data correspond to dto's decorators
    const object = plainToClass(metatype, value);
    const errors = await validate(object);

    if (errors.length > 0) {
      console.log(errors);

      throw new BadRequestException('Campos Invalidos');
    }

    return value;
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Boolean, Number, Array, Object];
    return !types.includes(metatype);
  }
}
