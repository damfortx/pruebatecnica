import { PartialType } from '@nestjs/mapped-types';
import { CreateEmployeeDto } from './create-employee.dto';
import {
  IsString,
  IsDate,
  IsUppercase,
  Length,
  IsAlpha,
  IsDateString,
  MinDate,
  MaxDate,
  IsEmail,
  IsOptional,
  Matches,
} from 'class-validator';
/**
 * DTO for update user, here are the validation decorators
 */
export class UpdateEmployeeDto extends PartialType(CreateEmployeeDto) {
  id: string;

  @IsOptional()
  @IsString()
  @Length(0, 20)
  @IsAlpha()
  @IsUppercase()
  firstName: string;

  @IsOptional()
  @IsString()
  @Length(0, 50)
  @Matches(/^[A-Z ]*$/)
  middleName: String;

  @IsOptional()
  @IsString()
  @Length(0, 20)
  @Matches(/^[A-Z ]*$/)
  lastName: string;

  @IsOptional()
  @IsString()
  @Length(0, 20)
  @IsAlpha()
  @IsUppercase()
  secondLastName: string;

  @IsOptional()
  @IsString()
  country: string;

  @IsOptional()
  @IsString()
  documentType: string;

  @IsOptional()
  @IsString()
  @Matches(/^[a-zA-Z0-9-\-]*$/)
  document: string;

  @IsOptional()
  @IsEmail()
  @Length(14, 300)
  mail: string;

  @IsOptional()
  @IsString()
  state: string;

  @IsOptional()
  @IsString()
  area: string;

  @IsOptional()
  @IsDate()
  @MaxDate(new Date())
  @MinDate(new Date(new Date().setDate(new Date().getDate() - 30)))
  addmissionDate: Date;

  @IsDateString()
  updateDate: string;
}
