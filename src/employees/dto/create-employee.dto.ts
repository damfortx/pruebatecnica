import {
  IsString,
  IsDate,
  IsUppercase,
  Length,
  IsAlpha,
  IsDateString,
  MinDate,
  MaxDate,
  IsEmail,
  IsOptional,
  Matches,
  IsNumber,
} from 'class-validator';
/**
 * DTO for create user, here are the validation decorators
 */
export class CreateEmployeeDto {
  id: string;

  //string, only [A-Z] characters, 20 of length and uppercase
  @IsString()
  @Length(0, 20)
  @IsAlpha()
  @IsUppercase()
  firstName: string;

  //string, only [A-Z ] characters, 50 of length and uppercase, is optional
  @IsOptional()
  @IsString()
  @Length(0, 50)
  @Matches(/^[A-Z ]*$/)
  middleName: String;

  //string, only [A-Z ] characters, 20 of length and uppercase
  @IsString()
  @Length(0, 20)
  @Matches(/^[A-Z ]*$/)
  lastName: string;

  // string, only [A-Z] characters, 20 of length and uppercase
  @IsString()
  @Length(0, 20)
  @IsAlpha()
  @IsUppercase()
  secondLastName: string;
  //strign
  @IsString()
  country: string;
  //string
  @IsString()
  documentType: string;
  //string only characters [a-zA-Z0-9-\-]
  @IsString()
  @Matches(/^[a-zA-Z0-9-\-]*$/)
  document: string;

  //email length max 300
  @IsEmail()
  @Length(14, 300)
  mail: string;

  @IsString()
  area:string;
  
  @IsString()
  state: string;
  //date, maxDate, today, minDate 30 day ago
  @IsDate()
  @MaxDate(new Date())
  @MinDate(new Date(new Date().setDate(new Date().getDate() - 30)))
  addmissionDate: Date;

  @IsDateString()
  createdDate: string;
}
