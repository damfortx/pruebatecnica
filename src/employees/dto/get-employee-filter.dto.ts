/**
 * DTO to manage the filter and pagination
 */
export class EmployeeFilter {
  firstName: string;
  middleName: String;
  lastName: string;
  secondLastName: string;
  country: string;
  documentType: string;
  document: string;
  mail: string;
  state: string; 
  page:string;
  limit:string;
}
