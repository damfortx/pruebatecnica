import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type EmployeeDocument = Employee & Document;
/**
 * Mongodb model 
 */
@Schema()
export class Employee {
  @Prop()
  firstName: string;
  @Prop()
  middleName: String;
  @Prop()
  lastName: string;
  @Prop()
  secondLastName: string;
  @Prop()
  country: string;
  @Prop()
  documentType: string;
  @Prop()
  document: string;
  @Prop()
  mail: string;
  @Prop()
  state: string;
  @Prop()
  area: string;
  @Prop()
  addmissionDate: Date;
  @Prop()
  createdDate: string;
  @Prop()
  updateDate?: string;
}

export const EmployeSchema = SchemaFactory.createForClass(Employee);
