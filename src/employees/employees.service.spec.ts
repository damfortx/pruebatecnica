import { Test, TestingModule } from '@nestjs/testing';
import { EmployeesService } from './employees.service';

import { MongooseModule } from '@nestjs/mongoose';
import { Employee, EmployeSchema } from './schema/employee.schema';
describe('EmployeesService', () => {
  let service: EmployeesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(
          'mongodb+srv://nest:nest@cluster0.in0g2.mongodb.net/nest?retryWrites=true&w=majority',
        ),
        MongooseModule.forFeature([
          { name: Employee.name, schema: EmployeSchema },
        ]),
      ],
      providers: [EmployeesService],
    }).compile();

    service = module.get<EmployeesService>(EmployeesService); 
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
 
});
