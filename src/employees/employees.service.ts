import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import { EmployeeFilter } from './dto/get-employee-filter.dto';
import {
  EmployeeDocument,
  Employee as EmployeeMDB,
} from './schema/employee.schema';

//

/**
 * This class is the services for employees here are the methods for save new employee, update employee, find, etc
 * this version works with an array to store all employees
 */
@Injectable()
export class EmployeesService {
  constructor(
    @InjectModel(EmployeeMDB.name)
    private employeeModel: Model<EmployeeDocument>,
  ) {}

  private readonly logger = new Logger(EmployeesService.name);

  /**
   * This method adds a new employee in the array
   * @param {Employee} createEmployeeDto employee to save
   */
  create(employee: Employee) {
    this.logger.log('New employee save');
    return new this.employeeModel(employee).save();
  }

  /**
   * this method find all employees in mongodb
   */
  findAll() {
    return this.employeeModel.find().exec();
  }

  /**
   * this method filter the employees in the array and handle the pagination
   * @param filter this parameter has all filters and pagination information
   */
  async findEmployees(filter: EmployeeFilter) {
    // get the pagination attributes
    const { page: pageString, limit: limitString } = filter;
    let page = parseInt(pageString);
    let limit = parseInt(limitString);
    if (page < 0) page = 0;
    if (limit < 0) limit = 0;
    // get the index
    const index = page * limit;

    // delete pagination attributes from the object to unly gets the filtenring
    delete filter.page;
    delete filter.limit;

    //create regex for each filtering attribute
    Object.keys(filter).map((key) => {
      filter[key] = {
        $regex: new RegExp(filter[key], 'i'),
      };
    });

    //gets the employees from the db
    const result = this.employeeModel
      .find(filter, null, { skip: index, limit })
      .exec();
    const count = await this.employeeModel.countDocuments(filter).exec();
    return {
      data: count === 0 ? [] : await result,
      page: filter.page,
      total: count,
    };
  }

  /**
   * find employee by id
   * @param id id of a employee
   */
  findOne(id: string) {
    return this.employeeModel.findById(id).exec();
  }

  /**
   * update employee
   * @param id id of employee
   * @param updateEmployeeDto attributes to update
   */
  update(id: string, updateEmployeeDto: UpdateEmployeeDto) {
    this.logger.log('Employee updated');
    return this.employeeModel.findByIdAndUpdate(id, updateEmployeeDto,{useFindAndModify:false}).exec();
  }

  /**
   * delete employee
   * @param id id of employee
   */
  remove(id: string) {
    this.logger.log('Employee removed');
    return this.employeeModel.findByIdAndRemove(id, { useFindAndModify: true });
  }

  /**
   * this method verify if the document already exist
   * @param documentType documentType (Cedula,Pasaporte)
   * @param document document of an employee
   * @param id id of an employee
   */
  async verifyDocument(documentType, document, id) {
    //  find a employee with the document and document type that match with the parameters
    const employeesCount = await this.employeeModel
      .countDocuments({
        document: document,
        documentType: documentType,
        _id: { $ne: id },
      })
      .exec();

    if (employeesCount !== 0) return true;
    return false;
  }

  /**
   * this create the first part of the email
   * @param firstName first name of the employee
   * @param lastName  last name of the employee
   * @param id id of the employee
   */
  async createMail(firstName, lastName, id): Promise<string> {
    //if the user does not change their last name or first name no mail is generated
    const employee = await this.findOne(id);
    if (employee) {
      if (
        employee.firstName.toLowerCase() === firstName &&
        employee.lastName.toLowerCase() === lastName
      )
        return employee.mail.split('@')[0];
    }
    let mail = firstName + '.' + lastName;
    //create a regex to verify the first part of the mail
    let mailRegrex = new RegExp('^' + mail + '([.]?[0-9]?)(@)@*.*', 'i');
    //verify if there's an employer with the first part of the email to adds an id to the email
    const employees = await this.employeeModel
      .find({
        mail: { $regex: mailRegrex },
        _id: { $ne: id },
      })
      .limit(2)
      .sort({ mail: -1 })
      .exec();

    //if there's no employees with the same first part on the email theres no id
    if (employees.length === 0) return mail;
    //get the number of the last email
    const position = employees.length === 1 ? 0 : 1;
    const lastEmployeeMail = employees[position].mail
      .split('@')[0]
      .split('.')[2];
    //if there's no number this is the second mail
    if (lastEmployeeMail === undefined) return mail + '.' + '1';
    //if the previous email HAS a number, one will be added to the previous number
    return mail + '.' + (parseInt(lastEmployeeMail) + 1);
  }
}
