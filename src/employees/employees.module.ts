import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EmployeesService } from './employees.service';
import { EmployeesController } from './employees.controller';
import { Employee,EmployeSchema } from './schema/employee.schema';
/**
 * employees module, imports mongoose module
 */
@Module({
  imports:[  MongooseModule.forFeature([{ name: Employee.name, schema: EmployeSchema }])],
  controllers: [EmployeesController],
  providers: [EmployeesService]
})
export class EmployeesModule {}
