import { Test, TestingModule } from '@nestjs/testing';
import { EmployeesController } from './employees.controller';
import { EmployeesService } from './employees.service';

import { MongooseModule } from '@nestjs/mongoose';
import { EmployeeDocument, EmployeSchema } from './schema/employee.schema';
import { EmployeeFilter } from './dto/get-employee-filter.dto';
import { Employee } from './entities/employee.entity';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
describe('EmployeesController', () => {
  let controller: EmployeesController;
  let service: EmployeesService;
  let employeeID: string;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(
          'mongodb+srv://nest:nest@cluster0.in0g2.mongodb.net/nest?retryWrites=true&w=majority',
        ),
        MongooseModule.forFeature([
          { name: Employee.name, schema: EmployeSchema },
        ]),
      ],
      controllers: [EmployeesController],
      providers: [EmployeesService],
    }).compile();

    controller = module.get<EmployeesController>(EmployeesController);
    service = module.get<EmployeesService>(EmployeesService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  describe('findAll', () => {
    it('should return an array of employees', async () => {
      const result = [];
      jest.spyOn(service, 'findAll').mockImplementation(async () => result);

      expect(await controller.findAll(new EmployeeFilter())).toBe(result);
    });
  });
 
  describe('create', () => {
    it('should save an employee ', async () => {
 
      const employee = new CreateEmployeeDto();
      employee.firstName = 'MARIAA';
      employee.middleName = '';
      employee.lastName = 'DE LA CALLE';
      employee.secondLastName = 'PEREZ';
      employee.country = 'Colombia';
      employee.documentType = 'Cédula de Ciudadanía';
      (employee.document = '1a2w4'),
        (employee.mail = 'mariaa.delacalle@cidenet.com.co');
      employee.addmissionDate = new Date('2021-01-14T00:00:00.000Z');
      employee.area = 'Administración';
      employee.state = 'Activo';
      employee.createdDate = '2021-01-31T11:05';

      const employeeMongo = await controller.create(employee);
      employeeID = employeeMongo._id;
      expect(employeeMongo.mail).toBe(employee.mail);
    });
  });
  describe('update', () => {
    it('should update an employee', async () => {
      const employee = new UpdateEmployeeDto();
      employee.document="123456"
      await controller.update(employeeID,employee);
      const employeeMongo =await  controller.findOne(employeeID)
      
      expect(employeeMongo.document).toStrictEqual(employee.document);
    });
  });
  describe('delete', () => {
    it('should delete an employee', async () => { 
      
      const employeeMongo = await controller.remove(employeeID); 
      expect(employeeMongo._id).toStrictEqual(employeeID);
    });
  });
});
