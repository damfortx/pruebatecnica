import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { EmployeesService } from './employees.service';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { EmployeeFilter } from './dto/get-employee-filter.dto'; 
import { ValidationMailPipe } from './pipes/validation-mail.pipe';
import { ValidationDocumentPipe } from './pipes/validation-document.pipe';
import { ValidationPipe } from './pipes/validation.pipe'; 

/**
 * Controler for employees
 * handle the endpoint /employees
 */
@Controller('employees')
export class EmployeesController {
  constructor(private readonly employeesService: EmployeesService) {}

  /**
   * endpoint to verify a document is valid
   * @param document document of an employee
   * @param documentType documentType of a employee
   * @param id id of a employee
   */
  @Get('/document')
  verifyDocument(
    @Query('document') document: string,
    @Query('documentType') documentType: string,
    @Query('id') id: string,
  ) {
    return this.employeesService.verifyDocument(documentType, document, id);
  }

/**
 * endpoint to create the first part of an email
 * @param firstName 
 * @param lastName 
 * @param id 
 */
  @Get('/mail')
  createMail(
    @Query('firstName') firstName: string,
    @Query('lastName') lastName: string,
    @Query('id') id: string,
  ) {
    return this.employeesService.createMail(firstName, lastName, id);
  }

/**
 * endpoint to save employee, before save the employe, 
 * there's a few pipes to validate the data, like the mail, document, and the structure of the data
 * @param createEmployeeDto dto of employee
 */
  @Post()
  async create(
    @Body(ValidationPipe, ValidationMailPipe, ValidationDocumentPipe)
    createEmployeeDto: CreateEmployeeDto,
  ) {
    return this.employeesService.create(createEmployeeDto);
  }

/**
 * endpoint to get all employees or get employees by filtering
 * @param employeeFilter 
 */
  @Get()
  findAll(@Query() employeeFilter: EmployeeFilter) {
    if (Object.keys(employeeFilter).length)
      return this.employeesService.findEmployees(employeeFilter);
    return this.employeesService.findAll();
  }

  /**
   * endpoint to get a enployee by id
   * @param id 
   */
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.employeesService.findOne(id);
  }

/**
 * endpoint to update employee, before update the employe,
 *  there's a few pipes to validate the data, like the mail, document, and structure of the data
 * @param id 
 * @param updateEmployeeDto 
 */
  @Put(':id')
  update(
    @Param('id') id: string,
    @Body(ValidationPipe, ValidationMailPipe, ValidationDocumentPipe)
    updateEmployeeDto: UpdateEmployeeDto,
  ) {
    return this.employeesService.update(id, updateEmployeeDto);
  }

  /**
   * endpint to delete employee by id
   * @param id 
   */
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.employeesService.remove(id);
  }
}
