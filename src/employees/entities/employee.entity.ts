/**
 * Entity employee
 *has all attributes of an employee
 */
export class Employee {
  id: string;
  firstName: string;
  middleName: String;
  lastName: string;
  secondLastName: string;
  country: string;
  documentType: string;
  document: string;
  mail: string;
  area: string;
  state: string;
  addmissionDate: Date;
  createdDate: string;
  updateDate?: string;
}
