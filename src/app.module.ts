import { Module } from '@nestjs/common';
import { EmployeesModule } from './employees/employees.module';
import { MongooseModule } from '@nestjs/mongoose';

//Main module, imports employees module, and mongo connection
@Module({
  imports: [EmployeesModule,MongooseModule.forRoot('mongodb+srv://nest:nest@cluster0.in0g2.mongodb.net/nest?retryWrites=true&w=majority')],
  controllers: [],
  providers: [],
})
export class AppModule {}
